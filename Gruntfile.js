
module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-watch');
 grunt.loadNpmTasks('grunt-sass');


grunt.initConfig({
     
        uglify: {

            build: {
                src: '_js/**.js',
                dest: 'js/script.min.js'
            } //my_targer where to export the file

        },

   sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'scss',
                    src: ['*.scss'],
                    // dest: 'assets/css',
                    ext: '.css'
                }]
            }
        },
   

    
        watch: {
            options: {
                livereload: true
            },

            scripts: {
                files: ['_js/*.js'],
                tasks: ['uglify']
            }, //scripts
            sass: {
                files: ['scss/**/*.scss'],
                tasks: ['sass']
            }, //sass
            html: {
                files: ['*.html']
            }


        } //watch

});

grunt.registerTask('default', ['watch']);

}

